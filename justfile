# runs TF init with GitLab HTTP Backend
init:
	#!/usr/bin/env sh
	terraform init

# runs TF plan
plan:
	#!/usr/bin/env sh
	terraform plan

# runs TF apply with auto-approve
apply:
	#!/usr/bin/env sh
	terraform apply -auto-approve

# generates json output file for KubeOne
out:
	#!/usr/bin/env sh
	terraform output -json > tf.json

# runs KubeOne
kubeone:
	#!/usr/bin/env sh
	ssh-add

	kubeone apply -m kubeone.yaml -t tf.json

# runs all TF stages and creates the cluster with KubeOne
cluster-create: init plan apply out kubeone

# get KUBECONFIG from KubeOne
kubeconfig:
	#!/usr/bin/env sh
	kubeone kubeconfig -m kubeone.yaml -t tf.json > .kubeconfig

# runs Kubeone reset & TF destroy
destroy:
	#!/usr/bin/env sh
	kubeone reset --manifest kubeone.yaml -t tf.json

	terraform destroy
